package com.example.color;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;



public class Test1 extends AppCompatActivity {
    public Button ex;
    public ImageButton home;
    public  RadioGroup option1;
    private RadioButton optionButton;
    public int num=10;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test1);
        // public ButtonRectangle ex;

        ex =(Button) findViewById(R.id.checkBtn15);
        home =(ImageButton) findViewById(R.id.img1);
        option1=(RadioGroup) findViewById(R.id.radioButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);

            }
        });


        ex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = option1.getCheckedRadioButtonId();
                optionButton = (RadioButton) findViewById(selectedId);
                String optxt= (String) optionButton.getText();
//change here wtr number
                if (optxt.equals("Number 07"))
                {

                    new AlertDialog.Builder(Test1.this)
                            .setTitle("Delete entry")
                            .setMessage("Are you sure you want to delete this entry?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                   /* Dialog dialog = new Dialog(Test1.this, "Verified","Those with normal vison can see this"  );
                    dialog.show();*/
                }
                else
                {


                    new AlertDialog.Builder(Test1.this)
                            .setTitle("Delete entry")
                            .setMessage("Are you sure you want to delete this entry?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                 /*   Dialog dialog = new Dialog(Test1.this, "Warning","The Majority of color blind people can not see this  number carefully"  );
                    dialog.show();*/
                }


            }
        });


    }
}
