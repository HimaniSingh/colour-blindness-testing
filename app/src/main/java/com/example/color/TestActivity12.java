package com.example.color;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class TestActivity12  extends AppCompatActivity {
    private ImageButton btnNext, btnBack, btnHome;
    private Button btnCheck;
    private EditText inputEd;
    private LinearLayout hiddenlayout;
    private RelativeLayout mainLayout;
    //    public int num2;
//    public  int num;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test12);

        btnBack = (ImageButton) findViewById(R.id.backBtn12);
        btnHome = (ImageButton) findViewById(R.id.homeBtn12);
        btnNext = (ImageButton) findViewById(R.id.nextBtn12);

        btnCheck = (Button) findViewById(R.id.checkBtn12);
        inputEd = (EditText) findViewById(R.id.input12);
        hiddenlayout = (LinearLayout) findViewById(R.id.showLayout12);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout12);
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) { count = extras.getInt("count"); }
//        else { num=9; }
        // Integer.parseInt(num2.toString());


        //num = Integer.decode(num2);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestActivity12.this, TestActivity8.class));
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Test_Activity15.class);
//                i.putExtra("num", num);
//startActivity(i);
                startActivityForResult(i, 0);
                //startActivity(new Intent(TestActivity8.this,TestActivity12.class));
                // startActivity(new Intent(TestActivity12.this,Test_Activity15.class));
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestActivity12.this, MainActivity.class));
            }
        });
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputstr = inputEd.getText().toString();
                TestActivity8.total++;

                if (inputstr.equals("12")) {
                  /*  Dialog dialog = new Dialog(TestActivity12.this,"Congratz", "You have identified correctly");
                    dialog.show();
                    Toast.makeText(TestActivity12.this,"Correct Answer",Toast.LENGTH_SHORT).show();*/
                    Toast.makeText(TestActivity12.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                    TestActivity8.count++;
                    startActivity(new Intent(TestActivity12.this, Test_Activity15.class));

                }  else {
//                    num=num-1;
                    startActivity(new Intent(TestActivity12.this, Test_Activity15.class));
                    mainLayout.setVisibility(View.INVISIBLE);
                    hiddenlayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Color Blindness Testing ");
                String shareMessage = "\nHi, Download this cool & fast performance Color Blindness Testing App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {

            }
        } else if (id == R.id.rate) {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        } else if (id == R.id.more) {
            // https://play.google.com/store/apps/developer?id=Appz+machine
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
            startActivity(intent);
        } else if (id == R.id.privacy) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
            startActivity(browserIntent);
        }

        return super.onOptionsItemSelected(item);

    }
}