package com.example.color;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class TestActivity8 extends AppCompatActivity {
    private ImageButton btnNext, btnBack, btnHome;
    private Button btnCheck;
    private EditText inputEd;
    private LinearLayout hiddenlayout;
    private RelativeLayout mainLayout;
    ///public  int num=10;
    public static int count = 0;
    public static int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test8);

        btnBack = (ImageButton) findViewById(R.id.backBtn8);
        btnHome = (ImageButton) findViewById(R.id.homeBtn8);
        btnNext = (ImageButton) findViewById(R.id.nextBtn8);

        btnCheck = (Button) findViewById(R.id.checkBtn8);
        inputEd = (EditText) findViewById(R.id.input8);
        hiddenlayout = (LinearLayout) findViewById(R.id.showLayout8);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout8);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestActivity8.this, MainActivity.class));
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TestActivity12.class);
                //i.putExtra("num", num);
//startActivity(i);
                startActivityForResult(i, 0);
                //startActivity(new Intent(TestActivity8.this,TestActivity12.class));
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestActivity8.this, MainActivity.class));
            }
        });
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputstr = inputEd.getText().toString();
                total++;
                if (inputstr.equals("8")) {
                 /*   new AlertDialog.Builder(TestActivity8.this)
                            .setTitle("Congratz")
                            .setMessage("You have identified correctly")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation

                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();*/
                    Toast.makeText(TestActivity8.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(TestActivity8.this, TestActivity12.class));
                    count++;
                 /*   Dialog dialog = new Dialog (TestActivity8.this, "Congratz", "You have identified correctly");
                    dialog.show();
                    Toast.makeText(TestActivity8.this,"Correct Answer",Toast.LENGTH_SHORT).show();*/
                } else {
//                    Toast.makeText(TestActivity8.this,"Wrong Answer",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(TestActivity8.this, TestActivity12.class));
                    mainLayout.setVisibility(View.INVISIBLE);
                    hiddenlayout.setVisibility(View.VISIBLE);


                }
//
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Color Blindness Testing ");
                String shareMessage = "\nHi, Download this cool & fast performance Color Blindness Testing App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {

            }
        } else if (id == R.id.rate) {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        } else if (id == R.id.more) {
            // https://play.google.com/store/apps/developer?id=Appz+machine
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
            startActivity(intent);
        } else if (id == R.id.privacy) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
            startActivity(browserIntent);
        }

        return super.onOptionsItemSelected(item);

    }
}


