package com.example.color;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    public Button test1;
    public Button ex;
    public Button ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        test1 =findViewById(R.id.test);
        test1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent i=new Intent(MainActivity.this,TestActivity8.class);
                startActivity(i);

            }
        });

        ex =findViewById(R.id.Exercise);
        ex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent i=new Intent(getApplicationContext(),exercise.class);
                startActivity(i);

            }
        });

        ab = findViewById(R.id.about);
        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent i=new Intent(getApplicationContext(),About.class);
                startActivity(i);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Typing Testing");
                String shareMessage = "\nHi, Download this cool & fast performance Typing Testing App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {

            }
        }else if (id == R.id.rate) {

            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
            }
            catch (ActivityNotFoundException e){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
            }
        }else if(id==R.id.more){
            // https://play.google.com/store/apps/developer?id=Appz+machine
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
            startActivity(intent);
        }else if (id == R.id.privacy) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
            startActivity(browserIntent);
        }

        return super.onOptionsItemSelected(item);
    }

}
